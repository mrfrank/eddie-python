#
# SPDX-License-Identifier: Apache-2.0
#
# SPDX-FileCopyrightText: Huawei Inc.
#
import aiocoap.resource as resource
import aiocoap
from aiocoap.util import linkformat
from aiocoap import error
from remote_resource import RemoteResource

import logging

def link_format_from_message(message):
    """Convert a response message into a LinkFormat object

    This expects an explicit media type set on the response (or was explicitly requested)
    """
    certain_format = message.opt.content_format
    if certain_format is None:
        certain_format = message.request.opt.accept
    try:
        if certain_format == aiocoap.ContentFormat.LINKFORMAT:
            return linkformat.parse(message.payload.decode('utf8'))
        else:
            raise error.UnsupportedContentFormat()
    except:
        raise error.BadRequest()

class EddieEndpoint:
    def __init__(self, endpoint_name: str):
        # Resource tree creation
        self.root = resource.Site()
        self.root.add_resource(['.well-known', 'core'],
                resource.WKCResource(self.root.get_resources_as_linkheader))
        self.resource_directory_url = None
        self.endpoint_name = endpoint_name
    
    def add_resource(self, path, resource):
        return self.root.add_resource(path, resource)
    
    async def publish_resources_to_rd(self):
        if not self.resource_directory_url:
            await self.discover_resource_directory()
        
        resources_link_format = self.root.get_resources_as_linkheader()
        link_format_string = str(resources_link_format).encode('utf8')
        request = aiocoap.Message(code=aiocoap.POST, 
                                    payload=link_format_string, 
                                    uri=f"{self.resource_directory_url[0]}{self.resource_directory_url[1]}?ep={self.endpoint_name}",
                                    content_format=40)
        try:
            response = await self.server_context.request(request).response
        except Exception as e:
            logging.error('Failed to publish resources to resource directory')
            logging.error(e)
        else:
            logging.debug('Publish resources response: %s\n%r'%(response.code, response.payload))


    async def start_server(self):
        port = 8683
        found_available_port = False
        while not found_available_port:
            try:
                self.server_context = await aiocoap.Context.create_server_context(self.root, bind=("::", port))
                found_available_port = True
            except OSError as e:
                port += 1
        
        if found_available_port: print(f"server bound to port: {port}")
        else: print("could not start server")

    async def discover_resource_directory(self):
        # resource directory is discovered by sending a request to .well-known/core following a heuristic:
        # first of all it looks in localhost, if it's not found in localhost sends a multicast request
        async def send_discovery(context, address):
            request = aiocoap.Message(code=aiocoap.GET, 
                                        uri=f"coap://{address}/.well-known/core?rt=core.rd",
                                        content_format=40)
            try:
                response = await context.request(request).response
            except Exception as e:
                logging.error(f'Could not send discovery request at address: {address}')
                logging.error(e)
                return None

            logging.debug('Discover rd response: %s\n%r'%(response.code, response.payload))

            parsed = link_format_from_message(response)
            if not parsed.links: return None
                
            remote = request.remote.sockaddr
            base = f"coap://[{remote[0]}]:{remote[1]}"
            path = parsed.links[0].href

            if not base or not path: return None
            return (base, path)
        
        rd_address_heuristics = ["[::1]", "127.0.0.1", "[FF05::FD]"]
        for address in rd_address_heuristics:
            rd_url = await send_discovery(self.server_context, address)
            if rd_url:
                self.resource_directory_url = rd_url
                break

    async def get_resources_from_rd(self, resource_type: str = None):
        # lookup resources from resource directory that was previously discovered
        
        if not self.resource_directory_url:
            logging.error("resource directory not discovered")
            return None 

        query = ""
        if resource_type:
            query = f"?rt={resource_type}"

        request = aiocoap.Message(code=aiocoap.GET, 
                                    uri=f"{self.resource_directory_url[0]}/rd-lookup/res{query}",
                                    content_format=40)
        try:
            response = await self.server_context.request(request).response
        except Exception as e:
            logging.error('Failed to retrieve resources from resource directory')
            logging.error(e)
            return None
        
        logging.debug('Get resources response: %s\n%r'%(response.code, response.payload))
        parsed = link_format_from_message(response)
        remote_resources = [RemoteResource(link, self.server_context) for link in parsed.links]
        return remote_resources