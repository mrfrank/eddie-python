#
# SPDX-License-Identifier: Apache-2.0
#
# SPDX-FileCopyrightText: Huawei Inc.
#
import aiocoap
import aiocoap.resource as resource
from aiocoap.cli.rd import StandaloneResourceDirectory
import asyncio
import argparse

async def main():
    parser = argparse.ArgumentParser(description='CoAP resource directory - Eddie Project.')
    parser.add_argument("--interface", help="Network interface to join the multicast group")
    args = parser.parse_args()

    multicast_interfaces = [args.interface] if args.interface else []

    root = resource.Site()
    context = await aiocoap.Context.create_server_context(root, multicast=multicast_interfaces)

    StandaloneResourceDirectory.rd_path = ["rd"]
    StandaloneResourceDirectory.ep_lookup_path = ["rd-lookup", "ep"]
    StandaloneResourceDirectory.res_lookup_path = ["rd-lookup", "res"]
    site = StandaloneResourceDirectory(context=context)

    context.serversite = site

    # Run forever
    await asyncio.get_running_loop().create_future()

if __name__ == "__main__":
    asyncio.run(main())