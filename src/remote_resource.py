import aiocoap
from aiocoap.util import linkformat

class RemoteResource(aiocoap.resource.Resource):
    def __init__(self, resource_link: linkformat.Link, client_context: aiocoap.Context):
        self.client_context = client_context
        self.resource_link = resource_link
        self.rt = resource_link.rt if resource_link.rt else "eddie.remote"

    def __str__(self) -> str:
        return str(self.resource_link)

    async def render_get(self, request):
        request = aiocoap.Message(code=aiocoap.GET, uri=self.resource_link.href)

        try:
            response = await self.client_context.request(request).response
        except Exception as e:
            print(e)
            return None
            
        return response

    async def render_post(self, request):
        request = aiocoap.Message(code=aiocoap.POST, uri=self.resource_link.href)

        try:
            response = await self.client_context.request(request).response
        except Exception as e:
            print(e)
            return None
            
        return response

    async def render_put(self, request):
        request = aiocoap.Message(code=aiocoap.PUT, uri=self.resource_link.href)

        try:
            response = await self.client_context.request(request).response
        except Exception as e:
            print(e)
            return None
            
        return response

    async def render_delete(self, request):
        request = aiocoap.Message(code=aiocoap.DELETE, uri=self.resource_link.href)

        try:
            response = await self.client_context.request(request).response
        except Exception as e:
            print(e)
            return None
            
        return response