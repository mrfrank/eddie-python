#
# SPDX-License-Identifier: Apache-2.0
#
# SPDX-FileCopyrightText: Huawei Inc.
#
import asyncio
from eddie_endpoint import EddieEndpoint
import aiocoap
import logging
from pprint import pprint

logging.getLogger().setLevel(logging.DEBUG)

ENDPOINT_NAME = "eddie-temp-node"

class EddieTemp(aiocoap.resource.Resource):
    def __init__(self):
        super().__init__()
        self.lamp_status = False
        self.rt = "eddie.temp"
        self.temperature = b"23"

    async def render_get(self, request):
        return aiocoap.Message(payload=self.temperature)


async def main():

    my_endpoint = EddieEndpoint(ENDPOINT_NAME)
    my_endpoint.add_resource(['temp1'], EddieTemp())
    await my_endpoint.start_server()
    await my_endpoint.publish_resources_to_rd()
    
    discovered_resources = await my_endpoint.get_resources_from_rd(resource_type="eddie.lamp")
    print("discovered resoruces:")
    for resource in discovered_resources:
        lamp_status = await resource.render_get(aiocoap.Message())
        print(f"{resource} - lamp status: {lamp_status.payload}")

    # Run forever
    await asyncio.get_running_loop().create_future()

if __name__ == "__main__":
    asyncio.run(main())